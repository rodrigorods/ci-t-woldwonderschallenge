package com.ciandt.rodrigo.worldwonderinfochallenge.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ciandt.rodrigo.worldwonderinfochallenge.R;
import com.ciandt.rodrigo.worldwonderinfochallenge.bean.LoggedUser;
import com.ciandt.rodrigo.worldwonderinfochallenge.bean.ResponseEnvelope;
import com.ciandt.rodrigo.worldwonderinfochallenge.listeners.OnLoadListener;
import com.ciandt.rodrigo.worldwonderinfochallenge.services.WorldWondersServices;

import org.json.JSONException;

import java.io.IOException;

public class LogIntoSystemTask extends AsyncTask<Void, Void, ResponseEnvelope<LoggedUser>>
{
    private Context context;
    private String email;
    private String password;
    private ProgressDialog dialog;
    private OnLoadListener listener;

    public LogIntoSystemTask(Context context, String email, String pass, OnLoadListener listener) {
        this.context = context;
        this.email = email;
        this.password = pass;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.warning_logging_in));
        dialog.show();
    }

    @Override
    protected ResponseEnvelope<LoggedUser> doInBackground(Void... params) {
        try {
            return WorldWondersServices.LogInApp(email, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ResponseEnvelope<LoggedUser> envelope) {
        super.onPostExecute(envelope);

        dialog.dismiss();

        if (envelope == null || envelope.getResponseCode() != 0) {
            listener.OnError(envelope);
        } else {
            listener.OnLoad(envelope);
        }
    }
}
