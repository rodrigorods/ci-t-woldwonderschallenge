package com.ciandt.rodrigo.worldwonderinfochallenge.listeners;

import com.ciandt.rodrigo.worldwonderinfochallenge.bean.ResponseEnvelope;

public interface OnLoadListener {

    void OnLoad(ResponseEnvelope envelope);
    void OnError(ResponseEnvelope envelope);

}
